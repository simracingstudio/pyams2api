#!/usr/bin/env python
from setuptools import setup, find_packages

setup(name='pyams2api',
      version='0.1',
      description='Automobilista 2 API implementation for Python 3',
      author='Sim Racing Studio',
      author_email='engineering@simracingstudio.com',
      url='https://gitlab.com/simracingstudio/pypams2api/',
      keywords='games racing ams2 ams automobilista',
      packages=find_packages(),      
     )
